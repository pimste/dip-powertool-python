# IMPORTEREN VAN LIBARIES
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import plotly.offline as po
import openpyxl
 
# LEZEN VAN DATASET
customers_eindhoven = pd.read_csv('https://larscuny.info/projecten/dip/data/eindhoven_customers.csv')
customers_den_bosch = pd.read_excel('https://larscuny.info/projecten/dip/data/mockup-data-denbosch.xlsx', 'Customers') # CUSTOMER TABEL VAN EINDHOVEN (MOCKAROO)
locations = pd.read_csv('https://larscuny.info/projecten/dip/data/postcode_locatie.csv') # BEVAT DE EXACTE LONGITUDES & LATITUDES VAN POSTCODES
postcode_info = pd.read_csv('https://larscuny.info/projecten/dip/data/postcode_informatie.csv') # INFORMATIE OMTRENT POSTCODES (LEEFTIJD, GENDER, AFKOMST, WOZ WAARDE)
 
# DATASETS MERGEN [VLOOKUP]
# Deze datasets worden samengevoegd door middel van de postcode van de customers. Dit houdt in dat de customer table hetzelfde blijft,
# maar de bijbehorende gegevens van bijvoorbeeld de postcode latitude en longitude gekopieerd worden naar de postcode van de customer 
# in een nieuwe kolom
 
denbosch_nieuw = pd.DataFrame(customers_den_bosch, columns = ['city', 'postalcode'])
eindhoven_nieuw = pd.DataFrame(customers_eindhoven, columns = ['city', 'postalcode'])
 
steden_customers = denbosch_nieuw.append(eindhoven_nieuw, ignore_index=True)
 
print(steden_customers)
 
full_customers = pd.merge(
    steden_customers,
    locations,  
    on ='postalcode',  
    how ='left'
) 
 
 
# Kijk in de terminal voor het resultaat van de merges hierboven
# print(df)
 
# OPZET VAN VISUALISATIE
# Dit is slechts de opzet van de visualisatie, het gebruikt als dataframe de df2 die we net hebben samengevoegd.
# Daarna worden de df2 latitude en longitude aangeroepen van elke losse klant en weergegeven op de kaart. 
fig = px.scatter_mapbox(
    full_customers,
    lat='latitude',
    lon='longitude',
    color='city',
    hover_name="postalcode",
    zoom=9
)


 
# Dit zijn de oranje lijnen die het gebied om Eindhoven heen markeren. (Niet zo interessant)
fig.add_trace(go.Scattermapbox(
    mode = "markers+lines",
    lon = [5.4, 5.75, 5.75, 5.05, 5.4], 
    lat = [51.85, 51.79, 51.64, 51.56, 51.85],
    marker = {'size': 10}
))
 
fig.add_trace(go.Scattermapbox(
    mode = "markers+lines",
    lon = [5.5, 6, 5.9, 5.1, 5.5], 
    lat = [51.7, 51.77, 51.33, 51.33, 51.7],
    marker = {'size': 10}
))
 
# LAYOUT VAN DE VISUALISATIE
fig.update_layout(
    mapbox_style="carto-positron", # 'open-street-map', 'white-bg', 'carto-positron', 'carto-darkmatter', 'stamen-terrain', 'stamen-toner', 'stamen-watercolor'
    font_family="Axiforma",
    font_color="#002072",
    title_font_family="Axiforma",
    title_font_color="#002072",
    legend_title_font_color="#002072",
    font_size=20,
    geo_resolution=50,
)
 
# OPEN VISUALISATIE
# fig.show()
po.plot(fig, filename="9_city_customers.html")
# EXPORTEER NAAR HTML EN OPEN VISUALISATIE
# pio.write_html(fig, file='7_eindhoven_city.html', auto_open=True)