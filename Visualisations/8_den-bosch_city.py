# IMPORTEREN VAN LIBARIES
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import plotly.offline as po
import openpyxl


# LEZEN VAN DATASET
customers = pd.read_excel('mockup-data-denbosch.xlsx', 'Customers') # CUSTOMER TABEL VAN EINDHOVEN (MOCKAROO)
locations = pd.read_csv('https://larscuny.info/projecten/dip/data/postcode_locatie.csv') # BEVAT DE EXACTE LONGITUDES & LATITUDES VAN POSTCODES
postcode_info = pd.read_csv('https://larscuny.info/projecten/dip/data/postcode_informatie.csv') # INFORMATIE OMTRENT POSTCODES (LEEFTIJD, GENDER, AFKOMST, WOZ WAARDE)

# DATASETS MERGEN [VLOOKUP]
# Deze datasets worden samengevoegd door middel van de postcode van de customers. Dit houdt in dat de customer table hetzelfde blijft,
# maar de bijbehorende gegevens van bijvoorbeeld de postcode latitude en longitude gekopieerd worden naar de postcode van de customer 
# in een nieuwe kolom
df = pd.merge(
    customers,  
    locations,  
    on ='postalcode',  
    how ='left'
) 

df2 = pd.merge(
    df,
    postcode_info,
    on = 'postalcode',
    how = 'left'
)

# Kijk in de terminal voor het resultaat van de merges hierboven
print(df2)

# OPZET VAN VISUALISATIE
# Dit is slechts de opzet van de visualisatie, het gebruikt als dataframe de df2 die we net hebben samengevoegd.
# Daarna worden de df2 latitude en longitude aangeroepen van elke losse klant en weergegeven op de kaart. 
fig = px.scatter_mapbox(
    df2,
    lat='latitude',
    lon='longitude',
    hover_name="postalcode",
    zoom=9
)

# Dit zijn de oranje lijnen die het gebied om Eindhoven heen markeren. (Niet zo interessant)
fig.add_trace(go.Scattermapbox(
    mode = "markers+lines",
    lon = [5.4, 5.75, 5.75, 5.05, 5.4], 
    lat = [51.85, 51.79, 51.64, 51.56, 51.85],
    marker = {'size': 10}
))

# LAYOUT VAN DE VISUALISATIE
fig.update_layout(
    mapbox_style="carto-positron", # 'open-street-map', 'white-bg', 'carto-positron', 'carto-darkmatter', 'stamen-terrain', 'stamen-toner', 'stamen-watercolor'
    font_family="Axiforma",
    font_color="#002072",
    title_font_family="Axiforma",
    title_font_color="#002072",
    legend_title_font_color="#002072",
    font_size=20,
    geo_resolution=50,
)


# OPEN VISUALISATIE
# fig.show()
po.plot(fig, filename="8_den-bosch_city.html")
# EXPORTEER NAAR HTML EN OPEN VISUALISATIE
# pio.write_html(fig, file='7_eindhoven_city.html', auto_open=True)




