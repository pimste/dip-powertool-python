# IMPORTEREN VAN LIBARIES
import plotly.express as px
import pandas as pd
import plotly.io as pio

# LEZEN VAN DATASET
transactions = pd.read_csv('https://larscuny.info/projecten/dip/data/transactions.csv')
customers = pd.read_csv('https://larscuny.info/projecten/dip/data/customers.csv')

transactions['date'] = pd.to_datetime(transactions['date'])
# OPZET VAN VISUALISATIE
fig = px.line(
    transactions, 
    x = "date", 
    y = "amount",
    color = "performance_id",
).update_xaxes(categoryorder="category ascending") # CATEGORIEEN ORDEREN

# LAYOUT VAN DE VISUALISATIE
fig.update_layout(
    font_family="Axiforma",
    font_color="#002072",
    title_font_family="Axiforma",
    title_font_color="#002072",
    legend_title_font_color="#002072",
    #font_size=20
)

# OPEN VISUALISATIE
#fig.show()
# EXPORTEER NAAR HTML EN OPEN VISUALISATIE
pio.write_html(fig, file='5_realistic_transaction_history.html', auto_open=True)
