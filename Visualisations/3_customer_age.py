# IMPORTEREN VAN LIBARIES
import plotly.express as px
import pandas as pd
import plotly.io as pio
# import plotly
import datetime


# LEZEN VAN DATASET
customers = pd.read_csv('https://larscuny.info/projecten/dip/data/customers.csv')
bevolking = pd.read_csv('https://larscuny.info/projecten/dip/data/bevolking_per_postcode.csv')
print(bevolking)
# CONVERTEREN GEBOORTE DATUM PANDAS FORMAAT
customers['age'] = pd.to_datetime(customers['birthdate'])

# FUNCTIE - CONVERTEREN GEBOORTE DATUM --> LEEFTIJD
def from_dob_to_age(x):
    today = datetime.date.today()
    return today.year - x.year - ((today.month, today.day) < (x.month, x.day))
# CONVERTEREN GEBOORTE DATUM --> LEEFTIJD 
customers['age'] = customers['age'].apply(lambda x: from_dob_to_age(x))#.astype(int)

# LEEFTIJDEN (GROEPEN VAN 5) DATAFRAME
leeftijden = {
    'age_group':[
        "0-5", "0-5", "0-5", "0-5", "0-5", "0-5", 
        "5-10", "5-10", "5-10", "5-10", "5-10", "5-10", 
        "10-15", "10-15", "10-15", "10-15", "10-15", "10-15",
        "15-20", "15-20", "15-20", "15-20", "15-20", "15-20", 
        "20-25", "20-25", "20-25", "20-25", "20-25", "20-25", 
        "25-30", "25-30", "25-30", "25-30", "25-30", "25-30", 
        "30-35", "30-35", "30-35", "30-35", "30-35", "30-35", 
        "35-40", "35-40", "35-40", "35-40", "35-40", "35-40", 
        "40-45", "40-45", "40-45", "40-45", "40-45", "40-45", 
        "45-50", "45-50", "45-50", "45-50", "45-50", "45-50", 
        "50-55", "50-55", "50-55", "50-55", "50-55", "50-55", 
        "55-60", "55-60", "55-60", "55-60", "55-60", "55-60", 
        "60-65", "60-65", "60-65", "60-65", "60-65", "60-65", 
        "65-70", "65-70", "65-70", "65-70", "65-70", "65-70", 
        "70-75", "70-75", "70-75", "70-75", "70-75", "70-75", 
        "75-80", "75-80", "75-80", "75-80", "75-80", "75-80", 
        "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", 
        "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", 
        "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", 
        "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", 
        "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", "80plus", "80plus"
    ],
    'age':[
        0, 1, 2, 3, 4, 5, 
        5, 6, 7, 8, 9, 10,
        10, 11, 12, 13, 14, 15, 
        15, 16, 17, 18, 19, 20,
        20, 21, 22, 23, 24, 25,
        25, 26, 27, 28, 29, 30,
        30, 31, 32, 33, 34, 35,
        35, 36, 37, 38, 39, 40,
        40, 41, 42, 43, 44, 45,
        45, 46, 47, 48, 49, 50,
        50, 51, 52, 53, 54, 55,
        55, 56, 57, 58, 59, 60,
        60, 61, 62, 63, 64, 65,
        65, 66, 67, 68, 69, 70,
        70, 71, 72, 73, 74, 75,
        75, 76, 77, 78, 79, 80,
        80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 
        101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120
    ],
} 

print(leeftijden)
leeftijden_frame = pd.DataFrame(data=leeftijden)
# DATASETS MERGEN
df = pd.merge(
    customers,  
    leeftijden_frame,  
    on ='age',
    how ='inner'
) 
print(df)
# OPZET VAN VISUALISATIE
fig = px.histogram(
    df, 
    x = "age_group", 
    color = "gender",
    color_discrete_map={
        'M': '#00C0B9',
        'V': '#FFF162'
    },
    #title = "In welke leeftijdscategorie vallen mijn bezoekers?"
).update_xaxes(categoryorder="category ascending") # CATEGORIEEN ORDEREN

# LAYOUT VAN DE VISUALISATIE
fig.update_layout(
    font_family="Axiforma",
    font_color="#002072",
    title_font_family="Axiforma",
    title_font_color="#002072",
    legend_title="Gender",
    legend_title_font_color="#002072",
    xaxis_title_text='Leeftijdsgroep',
    yaxis_title_text='Aantal',
    bargap=0.2,
    bargroupgap=0
)

# OPEN VISUALISATIE
#fig.show()
# plotly.offline.plot(fig, filename="dip.html")
# EXPORTEER NAAR HTML EN OPEN VISUALISATIE
pio.write_html(fig, file='3_customer_age.html', auto_open=True)
