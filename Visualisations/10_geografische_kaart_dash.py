# IMPORTEREN VAN LIBRARIES
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px
import pandas as pd
from numpy import int64

# LEZEN VAN DATASET
customers = pd.read_csv('https://larscuny.info/projecten/dip/data/customers.csv')
locations = pd.read_csv('https://larscuny.info/projecten/dip/data/postcode_locatie.csv')

# POSTCODE KOLOM WIJZIGING (VAN 5050AB (string) naar 5050 (int64))
customers.drop(customers.tail(2).index,inplace=True)
customers['postalcode'] = customers['postalcode'].astype(str).str[:4]
customers['postalcode'] = customers['postalcode'].astype(int64)

# DATASETS MERGEN [VLOOKUP]
# Als je bijvoorbeeld van de customers ook de bezochte voorstellingen zou willen weten, 
# zou je de transacties kunnen koppelen aan de performances en deze aan de voorstellingen. Dan kan je
# klanten filteren op het (meest, minst) bezochte genre.
customer_locations = pd.merge(
    customers,  
    locations,  
    on ='postalcode',  
    how ='left'
) 

# Ik print zelf meestal tijdens het coderen tussen door de dataset uit in de terminal.
# Dit doe ik omdat ik bijvoorbeeld snel inzichten kan krijgen van wat ik nog meer kan toevoegen als filter.
print(customer_locations)

# APP CREEEREN
app = dash.Dash()

# LAYOUT VAN DE APP
app.layout = html.Div(children=[
    html.H1(children='Geografische Kaart'),
    dcc.Dropdown(
        id='gender-dropdown',
        options=[
            {'label' : 'Alleen Mannen', 'value' : 'M'},
            {'label' : 'Alleen Vrouwen', 'value' : 'V'}
        ],
        value='M'
    ),
    # Hieronder een voorbeeld als je alles uit een kolom wilt ()
    #dcc.Dropdown(
    #    id='provincie-dropdown',
    #    options=[
    #        {'label' : i, 'value' : i}
    #        for i in customer_locations['provincie'].unique()
    #    ],
    #    value='Noord-Brabant'
    #),
    dcc.Graph(id='customer_graph')
])

# APP CALLBACKS
@app.callback(
    Output(component_id='customer_graph', component_property='figure'),
    Input(component_id='provincie-dropdown', component_property='value')
)

# VISUALISATIE UPDATEN
def update_graph(selected_customer):
    filtered_customer = customer_locations[customer_locations['gender'] == selected_customer]
    geographical_map = px.scatter_mapbox(
        filtered_customer, 
        lat='latitude',
        lon='longitude',
        hover_name="postalcode",
        size=filtered_customer['customer_id'].value_counts(),
        zoom=6
    )
    geographical_map.update_layout(
        mapbox_style="open-street-map", #open-street-map
        font_family="Axiforma",
        font_color="#002072",
        title_font_family="Axiforma",
        title_font_color="#002072",
        legend_title_font_color="#002072",
        font_size=20,
        geo_resolution=50,
    )
    return geographical_map


if __name__ == '__main__':
    app.run_server(debug=True)
