# IMPORTEREN VAN LIBARIES
from numpy import int64
import plotly.express as px
import pandas as pd
import plotly.io as pio

# LEZEN VAN DATASET
customers = pd.read_csv('https://larscuny.info/projecten/dip/data/customers.csv')
locations = pd.read_csv('https://larscuny.info/projecten/dip/data/postcode_locatie.csv')
postcode_info = pd.read_csv('https://larscuny.info/projecten/dip/data/postcode_informatie.csv')

# POSTCODE KOLOM WIJZIGING (VAN 5050AB (string) naar 5050 (int64))
customers.drop(customers.tail(2).index,inplace=True)
customers['postalcode'] = customers['postalcode'].astype(str).str[:4]
customers['postalcode'] = customers['postalcode'].astype(int64)

# DATASETS MERGEN [VLOOKUP]
df = pd.merge(
    customers,  
    locations,  
    on ='postalcode',  
    how ='left'
) 

df2 = pd.merge(
    df,
    postcode_info,
    on = 'postalcode',
    how = 'left'
)

# OPZET VAN VISUALISATIE
fig = px.scatter_mapbox(
    df2, 
    lat='latitude',
    lon='longitude',
    #color=df['customer_id'].value_counts(),
    color="woz_waarde",
    hover_name="postalcode",
    size='inwoners_totaal',
    zoom=6
)
# LAYOUT VAN DE VISUALISATIE
fig.update_layout(
    mapbox_style="open-street-map", #open-street-map
    font_family="Axiforma",
    font_color="#002072",
    title_font_family="Axiforma",
    title_font_color="#002072",
    legend_title_font_color="#002072",
    font_size=20,
    geo_resolution=50,
)


# OPEN VISUALISATIE
# fig.show()
# plotly.offline.plot(fig, filename="dip.html")
# EXPORTEER NAAR HTML EN OPEN VISUALISATIE
pio.write_html(fig, file='6_postcode_info.html', auto_open=True)
