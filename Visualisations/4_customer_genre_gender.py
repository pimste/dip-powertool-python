# IMPORTEREN VAN LIBARIES
import plotly.express as px
import pandas as pd
import plotly 

# LEZEN VAN DATASET
customers = pd.read_csv('https://larscuny.info/projecten/dip/data/customers.csv')
transactions = pd.read_csv('https://larscuny.info/projecten/dip/data/transactions.csv')
performances = pd.read_csv('https://larscuny.info/projecten/dip/data/performance.csv')
voorstellingen = pd.read_csv('https://larscuny.info/projecten/dip/data/voorstellingen.csv')

df = pd.merge(
    transactions,   
    performances,  
    on ='performance_id',  
    how ='left'
) 

df2 = pd.merge(
    df,
    voorstellingen,
    on = 'voorstelling_id',
    how = 'left'
)

df3 = pd.merge(
    df2,
    customers,
    on = 'customer_id',
    how = 'left'
)
print(df3)

fig = px.histogram(
    df3, 
    x = "genre", 
    #y = "amount.sum()",
    color = "gender",
    color_discrete_map={
        'M': '#00C0B9',
        'V': '#FFF162'
    },
).update_xaxes(categoryorder="category ascending") # CATEGORIEEN ORDEREN

# LAYOUT VAN DE VISUALISATIE
fig.update_layout(
    font_family="Axiforma",
    font_color="#002072",
    title_font_family="Axiforma",
    title_font_color="#002072",
    legend_title="Gender",
    legend_title_font_color="#002072",
    xaxis_title_text='Genre',
    yaxis_title_text='Aantal',
    bargap=0.2,
    bargroupgap=0
)

# OPEN VISUALISATIE
# fig.show()
plotly.offline.plot(fig, filename="4_customer_genre_gender.html")
# EXPORTEER NAAR HTML EN OPEN VISUALISATIE
# pio.write_html(fig, file='4_customer_genre_gender.html', auto_open=True)
