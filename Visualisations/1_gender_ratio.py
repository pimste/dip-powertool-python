# IMPORTEREN VAN LIBARIES
import plotly.express as px
import pandas as pd
import plotly.io as pio

# LEZEN VAN DATASET
df = pd.read_csv('https://larscuny.info/projecten/dip/data/customers.csv')

# OPZET VAN VISUALISATIE
fig = px.pie(
    df, 
    values='customer_id', 
    names='gender', 
    color='gender',
    color_discrete_map={
        'M': '#00C0B9',
        'V': '#FFF162'
    },
    labels='gender',
    hole=.6
)

fig.update_traces(
    textposition='inside', 
    textinfo='label+percent'
)

# LAYOUT VAN DE VISUALISATIE
fig.update_layout(
    font_family="Axiforma",
    font_color="#002072",
    title_font_family="Axiforma",
    title_font_color="#002072",
    legend_title="Gender",
    legend_title_font_color="#002072",
)

# OPEN VISUALISATIE
#fig.show()
# EXPORTEER NAAR HTML EN OPEN VISUALISATIE
pio.write_html(fig, file='1_gender_ratio.html', auto_open=True)
